<?php
//////////////////////////////
//
// admin_header.php
//  For use in shared
//  admin panel.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SIGN Admin</title>
<style type="text/css">
body{
text-align: center;
}

tr,td,th{
margin: auto;
text-align: center;
border: solid 1px black;
}

a{
letter-spacing: 1px;
margin: 2px;
text-decoration: none;
}

a:hover{
letter-spacing: 0px;
margin: 2px;
text-decoration: underline;
}
</style>
</head>
<body>

<div style="margin: auto; float: left; width: 100%; border: solid 1px black;"> <!-- header -->

<div style="float: left; margin: 4px;">
<a href="./index.php?a=1">Module List</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="./index.php?a=2">Add a Module</a>
</div>

<div style="float: right; margin: 4px;">
<a href="./index.php?a=7">Users</a> | <a href="./index.php?a=0">Logout of Admin Panel</a>
</div>

</div> <!-- end header -->

<br>

<?php
}//end check if user is admin
?>
