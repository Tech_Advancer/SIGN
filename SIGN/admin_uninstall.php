<?php
//////////////////////
//
//  uninstall.php
//  Included in admin panel.
//  Uninstalls a forum
//   module.
//  Expects GET value p.
//////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is an admin
if((isset($_GET['p'])) && (is_numeric(trim($_GET['p'])))){
//if a module number is set

$link = db_connect($database_url, $database_username, $database_password, $database_name);

$moduleNumber = trim($_GET['p']);

if((isset($_GET['s'])) && ($_GET['s']==1)){
//already asked if sure

	$query = 'SELECT name,location FROM shared_installations WHERE number=' . $moduleNumber;

        include('./admin_header.php');

        if($result = mysqli_query($link, $query)){
	        while($row = mysqli_fetch_object($result)){
			$moduleName = $row->name;
			$moduleLocation = $row->location;
		}
	}else{
		die("Error!");
	}
	include('.' . $moduleLocation . 'uninstall.php');
	include('./admin_footer.php');

	unset($query); unset($row); unset($result);
        mysqli_close($link);

}else{
//ask if sure

	$query = 'SELECT name,location FROM shared_installations WHERE number=' . $moduleNumber;

	include('./admin_header.php');

	if($result = mysqli_query($link, $query)){
	        while($row = mysqli_fetch_object($result)){
?>
<h1>Are you sure you want to delete <?php echo $row->name; ?>?</h1>
<h3>Using module: <?php echo $row->location; ?></h3>
<p>There is no way to recover it after this!</p>
<br><br>
<a href="./index.php?a=5&p=<?php echo $moduleNumber; ?>&s=1">YES</a>
<?php
        	}
	}

	include('./admin_footer.php');

	unset($query); unset($row); unset($result);
	mysqli_close($link);

} //end ask if sure
} //end if a module number is set
} //end check if user is an admin
?>
