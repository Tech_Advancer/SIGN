<?php
////////////////////////////////
// admin_login.php
// Included by index.php for admin's only!
//////////////////////////////

//check if is_admin is set and is 1 (true)
if((isset($_SESSION['is_admin_0'])) && ($_SESSION['is_admin_0']==1) && (isset($_SESSION['is_admin_1'])) && ($_SESSION['is_admin_1']==1)){

if((isset($_GET['a'])) && (is_numeric($_GET['a']))){ $apanelNumber =$_GET['a']; }

if((isset($_POST['userName'])) && (isset($_POST['password']))){ //if we are logging in

	$link = db_connect($database_url,$database_username,$database_password,$database_name);
	$userName = db_safe($_POST['userName'], $link);
	$password = $_POST['password'];
	$query = "SELECT number,password,banMonth,banYear,banDay,sharedAdmin FROM shared_users WHERE username=?";
    	$stmt = mysqli_stmt_init($link);
    if(mysqli_stmt_prepare($stmt, $query)){
        mysqli_stmt_bind_param($stmt, "s", $userName);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        mysqli_stmt_bind_result($stmt, $dbNumber, $dbPassword, $dbBanMonth, $dbBanYear, $dbBanDay, $dbSharedAdmin);
        mysqli_stmt_fetch($stmt);
	mysqli_stmt_close($stmt);
	unset($query);

	if($_SESSION['user_number']!=$dbNumber){ //user tried to login as another user
		session_destroy();
		die("Error!");
		//add to shared_warnings
	}

		if($dbSharedAdmin==1){ //double check if is admin (1=true; 0=false;)
			if(password_verify($password, $dbPassword)){ //check password
				if((strlen($dbBanYear)==0) && (strlen($dbBanMonth)==0) && (strlen($dbBanDay)==0)){//check if user hasn't been banned
					if(!password_needs_rehash($dbPassword, PASSWORD_DEFAULT, $hashOptions['options'])){ //if password doesn't need to be rehashed
						unset($password); unset($dbPassword); //unset this BEFORE including any more of the page!
						$_SESSION['is_admin_2']=1;
						?>

						<html>
						<head>
						<title>Admin Login</title>
						</head>
						<body>
						<br><br><br>
						<div style="margin: auto; text-align: center; border: solid 1px black;">
						Login Successful!<br><a href="./index.php?a=<?php echo $apanelNumber; ?>">Continue</a>
						</div>
						</body>
						</html>

						<?php
						}else{
							//if the password needs to be rehashed
							$query = "UPDATE shared_users SET password='" . password_hash($password, PASSWORD_DEFAULT, $hashOptions['options']) . "' WHERE number=" . $dbNumber;
							$stmt = mysqli_stmt_init($link);
							if(mysqli_stmt_prepare($stmt, $query)){
								mysqli_stmt_execute($stmt);
							}else{ //if prepare fails:
								die("Error!");
							}
							unset($link); unset($password); unset($dbPassword);
							$_SESSION['is_admin_2'] = 1;
							?>

							<html>
							<head>
							<title>Admin Login</title>
							</head>
							<body>
							<br><br><br>
							<div style="margin: auto; text-align: center; border: solid 1px black;">
							Login Successful!<br><a href="./index.php?a=<?php echo $apanelNumber; ?>">Continue</a>
							</div>
							</body>
							</html>

							<?php
						}
				}else{ //if user has been banned
				unset($password); unset($dbPassword);
				die("Error!");
				//Send a warning to shared_warnings whenever it is made in future versions
				//possible hacking since a non-admin got this far
				}
			}else{ //if password is wrong:
				unset($password);
				session_destroy();
				die("Login Failed!");
			}
		}else{//if sharedAdmin is wrong
			die("Error!");
			//Send a warning to shared_warnings whenever it is made in future versions
			//possible hacking since a non-admin got this far
		}
	}else{
		//Bad statement prepare
		die("Error!");
	}
}else{ //if we are NOT logging in (aka POST data NOT sent):
?>

<html>
<head>
<title>Admin Login</title>
</head>
<body>
<br><br><br>
<div style="margin: auto; text-align: center; border: solid 1px black;">
<h2>Admin Login:</h2>
<form action="./index.php?a=<?php echo $apanelNumber; ?>" method="POST">
<label>Username: <input type="text" name="userName"></label><br>
<label>Password: <input type="password" name="password"></label><br><br>
<input type="submit" value="submit">
</form>

</div>
</body>
</html>

<?php
} //if we are NOT logging in (aka POST data NOT sent)
}else{
//is_admin isn't set or isn't correct
die("Error!");
//Send a warning to shared_warnings whenever it is made in future versions
//possible hacking since a non-admin got this far
}
?>
