<?php
//////////////////////////////
//
// admin_change_path.php
//  For use in shared
//  admin panel. Changes a
//  module's name in the db.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

if((!isset($_GET['p'])) || (!is_numeric(trim($_GET['p'])))) //check if m [module #] isset and is a number
{
	session_destroy();
	die("Error!");
}

$moduleNumber = trim($_GET['p']);

if(isset($_POST['name'])){ //if the form was submitted

	$link = db_connect($database_url,$database_username,$database_password,$database_name);
	$updatedName = db_safe($_POST['name'], $link);
	$query = "UPDATE shared_installations SET name=? WHERE number=?";
	if(mysqli_connect_errno()){ die("Error!"); }

	$stmt = mysqli_stmt_init($link);

	if(mysqli_stmt_prepare($stmt, $query)){
		mysqli_stmt_bind_param($stmt, "si", $updatedName, $moduleNumber);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
		unset($link); unset($query);
	}else{
		die("Error!");
	}

	$message="Updated to " . $updatedName;
}


//get path for the module corrosponding to the given p value
$link = db_connect($database_url,$database_username,$database_password,$database_name);
$query = "SELECT name,location FROM shared_installations WHERE number=?";
if(mysqli_connect_errno()){ die("Error!"); }

$moduleNumber = trim($_GET['p']);
$stmt = mysqli_stmt_init($link);

if(mysqli_stmt_prepare($stmt, $query)){
        mysqli_stmt_bind_param($stmt, "i", $moduleNumber);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_store_result($stmt);
        mysqli_stmt_bind_result($stmt, $moduleName, $modulePath);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);
        unset($link); unset($query);
}else{ //if stmt prepare fails:
        session_destroy();
        die("Error!");
}//if stmt prepare

include("admin_header.php");
?>

<h1>Change <?php echo $moduleName; ?>'s Name</h1>

<?php
if((isset($message)) && (strlen($message)>0)){
?>
<div style="width: 95%; margin: auto; border: solid 3px red; color: red; font-size: 130%; text-align: center;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<div style="width: 95%; margin: auto;">
<form action="./index.php?a=3&p=<?php echo $moduleNumber; ?>" method="POST" autocomplete="off">
<label>Module Name: <input type="text" name="name" value="<?php echo $moduleName; ?>"></label><br>
Example: My Forums<br>
<br><br>
<input type="submit" value="Change">
</form>
</div>

<?php
include("admin_footer.php");
}//end check if user is admin
?>
