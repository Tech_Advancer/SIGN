<?php
///////////////////////////////////////////////////////////////////////
//shared_sessions.php
//Sessions handler for shared data that all modules will likely need.
///////////////////////////////////////////////////////////////////////

if(isset($load_check)){

session_start(); //No non-error output should go before this!

//check for ip ban here later. This version doesn't need it.

if((isset($_SESSION['user_number']))){ //check if logged in already

//check if user banned by id here later. This version doesn't need it.

//check if we are missing timezone or other things here and query them now! Add to session variables

//check for ip changes
if(!isset($_SESSION['user_ip'])){
	$_SESSION['user_ip']=get_ip();
}else{
	if($_SESSION['user_ip']!=get_ip()){ //if IP doesn't match session data; possible session hijack
		session_destroy();
		$_SESSION['user_number']=-1;
		$_SESSION['user_name']="Guest";
	}
}

}else{ //if not already logged in
  if((isset($_COOKIE['user_number'])) && (isset($_COOKIE['user_token']))){ //if a cookie is set
  echo "A cookie is set!"; //remove this!
  //check if cookie is correct & delete if they are wrong
  //add the ip to the database for login attempts
  //set session data if correct
  }else{ //if a cookie isn't set; AKA a guest:
    if((!isset($_SESSION['user_number'])) && (!isset($_SESSION['user_name']))){ //and other things like ip...
    $_SESSION['user_number']=-1;
    $_SESSION['user_name']="Guest"; //this needs to be setup for the language package file later for multiple languages

    $mysqli_link=db_connect($database_url,$database_username,$database_password,$database_name);
    $result=mysqli_query($mysqli_link,"SELECT intValue FROM shared_settings WHERE name='loadFirst'");
    $row=mysqli_fetch_assoc($result);
    $_SESSION['load_first']=$row["intValue"];
    mysqli_free_result($result);
    mysqli_close($mysqli_link);

	if((!isset($_SESSION['load_first'])) || (!is_numeric($_SESSION['load_first']))){
		die("No modules installed! An admin needs to install one via the admin control panel!");
	}

    } //END: if GUEST
  } //END: if cookie is set
} //END: if logged in already
}else{
if(headers_sent()){die(include($root . "/404.php"));} //if headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); //send headers if not sent already
die(include($root . "/404.php")); //die to custom 404 page
}

?>
