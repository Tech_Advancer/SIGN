<?php
//////////////////////////////
//
// admin_userEdit.php
//  For use in shared
//  admin panel. Lets admins
//  change user info.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

if((!isset($_GET['p'])) || (!is_numeric(trim($_GET['p'])))) //check if p [user] isset and is a number
{
	session_destroy();
	die("Error!1");
}

$userNumber = trim($_GET['p']);

$link = db_connect($database_url,$database_username,$database_password,$database_name);

if((isset($_POST['username'])) && (isset($_POST['emailAddress'])) && (isset($_POST['joinDate'])) && (isset($_POST['website'])) && (isset($_POST['sharedAdmin']))){ //if the form was submitted

	$message = "";
	$userName = db_safe($_POST['username'], $link);

	if((isset($_POST['userPassword'])) && (strlen($_POST['userPassword'])>0)){
	       	$password = db_safe($_POST['userPassword'], $link);
	      	$passwordVerify = db_safe($_POST['userPassword2'], $link);
		if((strlen($password)<8) || (strlen($password)>70)){ $message.='Your password MUST be between 8 and 70 characters long!<br>'; }
	        if(strcmp($password, $passwordVerify) !== 0){ $message.='Your password didn\'t match! It IS case sensitive!<br>'; }
	}

       	$email = db_safe($_POST['emailAddress']);

	if(!isset($_POST['aacc'])){ //if disable aac is checked
		$AAC="";
	}else{ //if not:
		$AAC=db_safe($_POST['aac'], $link);
	}

	$joinDate = db_safe($_POST['joinDate'], $link);
	$website = db_safe($_POST['website'], $link);

	if(is_numeric(trim($_POST['sharedAdmin']))){
		$sharedAdmin = $_POST['sharedAdmin'];
	}else{
		die("Error!2");
	}

	$modNotes = db_safe($_POST['modNotes'], $link);
	$banReason = db_safe($_POST['banReason'], $link);
	$banYear = db_safe($_POST['banYear'], $link);
	$banMonth = db_safe($_POST['banMonth'], $link);
	$banDay = db_safe($_POST['banDay'], $link);

	if($banYear<2015){ $banYear=NULL; }
	if($banMonth==0){ $banMonth=NULL; }
	if($banDay==0){ $banDay=NULL; }

     	if((strlen($userName)<3) || (strlen($userName)>25)){ $message.='Your username MUST be between 3 and 25 characters long!<br>'; }
     	if(strlen($email)<3){ $message.='You must enter your email address!<br>'; }

      	if(strlen($message)<1){ //if there isn't some other error already

		$conflicts=0;

		//Query to be sure that the username or email isn't already being used
		$query = 'SELECT number,username,emailAddress FROM shared_users WHERE username=' . $userName . ' OR emailAddress=' . $email;
		$query = mysqli_real_escape_string($link, $query);
		if($result = mysqli_query($link, $query)){
		        while($row = mysqli_fetch_object($result)){
				if($userNumber!=$row->number){
					$conflicts = $conflicts + 1;
					if($userName==$row->username){
						$message.='That username is already being used by another user.<br>';
					}
					if($email==$row->emailAddress){
						$message.='That email address is alread being used by another user.<br>';
					}
				}
			}
		}
		unset($query); unset($result); unset($row);

      	}//end if there isn't some other error already

       	if(strlen($message)==0){ //if there is no error
		if((isset($_POST['userPassword'])) && (strlen($_POST['userPassword'])>0)){
	                //hash password
	                $password = password_hash($password, PASSWORD_DEFAULT, $hashOptions['options']);

			//update shared users with password
			$query = "UPDATE shared_users SET username=?,password=?,joinDate=?,emailAddress=?,website=?,accountActivationCode=?,modNotes=?,banReason=?,banYear=?,banMonth=?,banDay=?,sharedAdmin=? WHERE number=?";
		        if(mysqli_connect_errno()){ die("Error!"); }
			$stmt = mysqli_stmt_init($link);

		        if(mysqli_stmt_prepare($stmt, $query)){
		                mysqli_stmt_bind_param($stmt, "ssssssssiiiii", $userName, $password, $joinDate, $email, $website, $AAC, $modNotes, $banReason, $banYear, $banMonth, $banDay, $sharedAdmin, $userNumber);
		                mysqli_stmt_execute($stmt);
		                mysqli_stmt_close($stmt);
		                unset($query);
		        }else{ //if stmt prepare fails
		                die("Error!3");
		        }//if stmt prepare
		}else{
			//update shared_users without password
			$query = "UPDATE shared_users SET username=?,joinDate=?,emailAddress=?,website=?,accountActivationCode=?,modNotes=?, banReason=?, banYear=?, banMonth=?, banDay=?, sharedAdmin=? WHERE number=?";
                        if(mysqli_connect_errno()){ die("Error!7"); }
                        $stmt = mysqli_stmt_init($link);

                        if(mysqli_stmt_prepare($stmt, $query)){
                                mysqli_stmt_bind_param($stmt, "sssssssiiiii", $userName, $joinDate, $email, $website, $AAC, $modNotes, $banReason, $banYear, $banMonth, $banDay, $sharedAdmin, $userNumber);
                                mysqli_stmt_execute($stmt);
                                mysqli_stmt_close($stmt);
                                unset($query);
                        }else{ //if stmt prepare fails
                                die("Error!<br>" . $query . "<br>" . mysqli_error($link));
                        }//if stmt prepare
		}

		$message.="<h1>Done!</h1>";
	}
}


//get user info
$query = "SELECT username,joinDate,lastLogin,emailAddress,website,accountActivationCode,modNotes,banReason,ipList,banYear,banMonth,banDay,sharedAdmin FROM shared_users WHERE number=?";

$stmt = mysqli_stmt_init($link);

if(mysqli_stmt_prepare($stmt, $query)){
        mysqli_stmt_bind_param($stmt, "i", $userNumber);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_store_result($stmt);
        mysqli_stmt_bind_result($stmt, $username, $userJoinDate, $userLastLogin, $userEmailAddress, $userWebsite, $userAAC, $userModNotes, $userBanReason, $userIP, $userBanYear, $userBanMonth, $userBanDay, $userSharedAdmin);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);
        unset($link); unset($query);
}else{ //if stmt prepare fails:
        die("Error!6");
}//if stmt prepare

include("admin_header.php");
?>

<h1>Edit <?php echo $username; ?>'s Settings</h1>

<?php
if((isset($message)) && (strlen($message)>0)){
?>
<div style="width: 95%; margin: auto; border: solid 3px red; color: red; font-size: 130%; text-align: center;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<div style="width: 95%; margin: auto;">
<form action="./index.php?a=8&p=<?php echo $userNumber; ?>" method="POST" autocomplete="off">
<label>Username: <input type="text" name="username" value="<?php echo $username; ?>"></label><br><br>

<label>Change Password: <input type="password" name="userPassword"></label><br>
<label>Password (Again): <input type="password" name="userPassword2"></label><br><br>

<label>Email Address: <input type="text" name="emailAddress" value="<?php echo $userEmailAddress; ?>"></label><br>
<label>Join Date: <input type="text" name="joinDate" value="<?php echo $userJoinDate; ?>"></label><br>
<label>Website: <input type="text" name="website" value="<?php echo $userWebsite; ?>"></label><br>
<?php
if(strlen($userAAC)>0){
?>
<b>Account isn't active!<label> Check this box to activate account <input type="checkbox" name="aacc"></label><br>
<input type="hidden" value="<?php echo $userAAC; ?>" name="aac">
<?php
}
?>
<br>
<label>Shared Admin: <select name="sharedAdmin">
<?php if($userSharedAdmin==1){?>
<option value="1">Admin</option>
<option value="2">Make Normal User</option>
<?php }else{ ?>
<option value="2">Normal User</option>
<option value="1">Make Admin</option>
<?php } ?>
</select></label>

<h3>Mod Stuff:</h3>
<p>Last Login: <?php echo $userLastLogin; ?><br>
Last Used IP: <?php echo $userIP; ?></p>

<label>Mod Notes:<br>
<textarea rows="10" cols="30" name="modNotes"><?php echo $userModNotes; ?></textarea></label><br><br>

<h3>Only Enter Data Here if Banning User.<br>Clear Data to Un-ban:</h3>

<label>Ban Reason:<br>
<textarea rows="10" cols="30" name="banReason"><?php echo $userBanReason; ?></textarea></label><br><br>

<label>Ban Year: <input type="text" name="banYear" value="<?php echo $userBanYear; ?>"></label><br>
<label>Ban Month: <input type="text" name="banMonth" value="<?php echo $userBanMonth; ?>"></label><br>
<label>Ban Day: <input type="text" name="banDay" value="<?php echo $userBanDay; ?>"></label><br>


<br><br>
<input type="submit" value="Edit">
</form>
</div>

<?php
include("admin_footer.php");
}//end check if user is admin
?>
