<?php
//////////////////////////////
//
// admin_module_list.php
//  For use in shared
//  admin panel.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

include("admin_header.php");
?>

<h1>Module List</h1>

<table style="width: 95%; margin: auto; border-spacing: 15px;">
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get which module Loads first:
$dbLoadFirst=-1;
$query = 'SELECT intValue FROM shared_settings WHERE name="loadFirst"';
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                $dbLoadFirst = $row->intValue;
        }
}
unset($query); unset($row); unset($result);

//Get all of the modules:
$query = 'SELECT number,name,location FROM shared_installations';
$query = mysqli_real_escape_string($link, $query);

if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
?>
		<tr><td>
		<a href="./index.php?m=<?php echo $row->number; ?>&a=1">(<?php echo $row->number . ") &nbsp;" . $row->name; ?></a><br>
		<a href="./index.php?a=5&p=<?php echo $row->number; ?>">Delete</a> &nbsp;|&nbsp;
		<a href="./index.php?a=3&p=<?php echo $row->number; ?>">Rename</a> &nbsp;|&nbsp;
		<a href="./index.php?a=4&p=<?php echo $row->number; ?>">Change Path</a> &nbsp;|&nbsp;
		<?php if($row->number==$dbLoadFirst){ ?>
		Loads First
		<?php }else{ ?>
		<a href="./index.php?a=6&p=<?php echo $row->number; ?>">Set to Load First</a>(This will logout all users.)
		<?php } ?>
		</td></tr>
<?php
        }
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

mysqli_close($link);
?>
</table>

<?php
include("admin_footer.php");
}//end check if user is admin
?>
