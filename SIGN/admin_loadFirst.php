<?php
//////////////////////////////
//
// admin_module_list.php
//  For use in shared
//  admin panel.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

if((isset($_GET['p'])) && (is_numeric(trim($_GET['p'])))){
	$moduleNumber = $_GET['p'];
}else{
	session_destroy();
	die("Error!");
}

include("admin_header.php");

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'UPDATE shared_settings SET intValue=' . $moduleNumber . ' WHERE name="loadFirst"';
if(!$result = mysqli_query($link, $query)){
        die("<h1>Error Updating!</h1>");
}else{
?>
<h1>Updated!</h1>
<?php
}
unset($query); unset($row); unset($result);
mysqli_close($link);

include("admin_footer.php");
}//end check if user is admin
?>
