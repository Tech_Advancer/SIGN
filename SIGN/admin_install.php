<?php
//////////////////////////////
//
// admin_install.php
//  For use in shared
//  admin panel. Serves as
//  a mid-screen to get the
//  installation path from
//  the user.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

$load = true;

if(isset($_GET['e'])){ //coming back from /module/install.php; loop back to it.
$load = false;
$moduleLocation = $_POST['moduleLocation'];
include('./admin_header.php');
include('.' . $moduleLocation . 'install.php');
include('./admin_footer.php');
}else{

if((isset($_POST['installPath'])) && (strlen($_POST['installPath'])>0)){ //if the form was submitted

	if(file_exists("." . $_POST['installPath'] . "install.php")){
		$load = false;
		$linkBack = "./index.php?a=2&e=1";
		$moduleLocation = $_POST['installPath'];
		include('./admin_header.php');
		include('.' . $moduleLocation . 'install.php');
		include('./admin_footer.php');
	}else{
		$message="Installation or install.php not found at " . $_POST['installPath'];
	}
}

if((isset($_POST['installPath2'])) && (strlen($_POST['installPath2'])>0)) { //if the form was submitted

        if(file_exists("." . $_POST['installPath2'] . "install.php")){
                $load = false;
                $linkBack = "./index.php?a=2&e=1";
                $moduleLocation = $_POST['installPath2'];
                include('./admin_header.php');
                include('.' . $moduleLocation . 'install.php');
                include('./admin_footer.php');
        }else{
                $message="Installation or install.php not found at " . $_POST['installPath2'];
        }
}

if($load==true){
	include("admin_header.php");
?>

<h1>Add a new Module</h1>

<?php
if((isset($message)) && (strlen($message)>0)){
?>
<div style="width: 95%; margin: auto; border: solid 3px red; color: red; font-size: 130%; text-align: center;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<div style="width: 95%; margin: auto;">
<form action="./index.php?a=2" method="POST" autocomplete="off">
<label>Module File Path: <input type="text" name="installPath"></label><br>
Example: <b>/path/path2/</b><br>The starting and ending / are required!
<br><br>
Or select one already in use:
&nbsp;
<select name="installPath2">
<option value="">None</option>
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT location FROM shared_installations';
$query = mysqli_real_escape_string($link, $query);

if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
?>
		<option value="<?php echo $row->location; ?>"><?php echo $row->location; ?></option>
<?php
        }
}
mysqli_free_result($result); unset($query); unset($row); unset($result);

mysqli_close($link);
?>
</select>
<br><br>
<input type="submit" value="Go">
</form>
</div>

<?php
include("admin_footer.php");
} //end if load=true
} //end if coming back from /module/install.php
}//end check if user is admin
?>
