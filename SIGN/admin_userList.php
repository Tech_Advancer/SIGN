<?php
//////////////////////////////
//
// admin_userList.php
//  For use in shared
//  admin panel.
//  List and edit users.
/////////////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is admin

include("admin_header.php");
?>

<h1>User List:</h1>

<table style="width: 95%; margin: auto; border-collapse: collapse;">
<tr>
<th>User (number) Name</th><th>SIGN Shared Rank</th><th>Email Address</th>
</tr>
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get all of the modules:
$query = 'SELECT number,username,emailAddress,sharedAdmin FROM shared_users';
$query = mysqli_real_escape_string($link, $query);

if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
?>
		<tr><td>
		<a href="./index.php?p=<?php echo $row->number; ?>&a=8">(<?php echo $row->number . ") &nbsp;" . $row->username; ?></a>
		</td><td>
		<?php if($row->sharedAdmin==1){ ?>
		Admin
		<?php }else{ ?>
		User
		<?php } ?>
		</td><td>
		<?php echo $row->emailAddress; ?>
		</td></tr>
<?php
        }
}else{
die("Error!");
}
unset($query); unset($row); unset($result);

mysqli_close($link);
?>
</table>

<?php
include("admin_footer.php");
}//end check if user is admin
?>
