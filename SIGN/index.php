<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
///////////////////////////////////////////////////////////////////////
//index.php (root)
//The first file loaded for everything in SIGN. Everything goes through this.
///////////////////////////////////////////////////////////////////////


$root=getcwd(); //Root directory that this file is placed in. Used for other documents to find root.
$load_check=1;  //A check to be sure all future files are loaded through here.


///////////////////////////////////////////////////////////////////////////////////
// Moving shared variables and changing this variable is a good idea for security!
//
// Absolute Location of shared_variables.php that usually is in root.
// Example:   $shared_variables_location="/home/jfraser/private_html/";
	$shared_variables_location="";
//
//////////////////////////////////////////////////////////////////////////////////

if(!file_exists($shared_variables_location . "shared_variables.php")){ //if SIGN hasn't been setup yet or config file is missing
	if(!file_exists("install.php")){ //if the installer is missing, we may have already installed and corrupted something
		die("Unable to find shared_variables.php. Please check your index.php settings!<br>A system administrator is most likely working on the website.");
		//These error messages need to be updated later to use some sort of language file for multiple language support.
	}else{
		//the installer is still there, we probably haven't installed yet
		die(include("install.php"));
	}
}else{
	//if our config file exists
	if(!include_once($shared_variables_location . "shared_variables.php")){ //try to include it, die on error
		die("Unable to load shared_variables.php. Please check your settings!<br>A system administrator is most likely working on the website. Please check back later!");
	}
}

if(!include_once("shared_functions.php")){
//These error messages need to be updated later to use some sort of language file for multiple language support:
die("Unable to find shared_functions.php.<br>A system administrator is most likely working on the website. Please check back later!");
}

if(!include_once("shared_sessions.php")){
//These error messages need to be updated later to use some sort of language file for multiple language support:
die("Unable to find shared_sessions.php.<br>A system administrator is most likely working on the website. Please check back later!");
}

$modulePath="";
//Get Module Path below:
if((isset($_GET['m'])) && (is_numeric(trim($_GET['m'])))) //check if m [module #] isset and is a number
{
	//load the module corrosponding to m
	$link = db_connect($database_url,$database_username,$database_password,$database_name);
	$query = "SELECT name,location FROM shared_installations WHERE number=?";
	if(mysqli_connect_errno()){ die("Error!"); }
	$moduleNumber = trim($_GET['m']);
	$stmt = mysqli_stmt_init($link);

	if(mysqli_stmt_prepare($stmt, $query)){
		mysqli_stmt_bind_param($stmt, "i", $moduleNumber);
		mysqli_stmt_execute($stmt);

		mysqli_stmt_store_result($stmt);
		mysqli_stmt_bind_result($stmt, $moduleName, $modulePath);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		unset($link); unset($query);
	}else{ //if stmt prepare fails:
		session_destroy(); //in case the user is trying to load an expired module
		die("Error Loading Website!");
	}//if stmt prepare

	if((!isset($modulePath)) || (strlen($modulePath)<1)){ session_destroy(); die("Error Loading Website!"); }
	include($root . $modulePath . "module.php");

}else{//if m isn't set or isn't a number:
	if((isset($_GET['a'])) && (is_numeric(trim($_GET['a'])))){ //if e isset
		//if e isset without an m, the user is trying to get to the admin panel
		if((isset($_SESSION['user_number'])) && ($_SESSION['user_number']>=0)){ //if user is logged in
			if((isset($_SESSION['is_admin_0'])) && ($_SESSION['is_admin_0']==1) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){ //if the user has gone through the admin login
				$admin_load_check = $_SESSION['user_number'];
				switch(trim($_GET['a'])){ //get and load appropriate e value admin page
					case 0: include('./admin_logout.php'); break;
					case 1: include('./admin_module_list.php'); break;
					case 2: include('./admin_install.php'); break;
					case 3: include('./admin_rename.php'); break;
					case 4: include('./admin_change_path.php'); break;
					case 5: include('./admin_uninstall.php'); break;
					case 6: include('./admin_loadFirst.php'); break;
					case 7: include('./admin_userList.php'); break;
					case 8: include('./admin_userEdit.php'); break;
					default: include('./admin_logout.php'); break;
				}
			}else{ //if user hasn't gone through the admin login
				/* QUERY DB FOR ADMIN STATUS */
				$link = db_connect($database_url, $database_username, $database_password, $database_name);
				$query = "SELECT sharedAdmin FROM shared_users WHERE number=?";
				if(mysqli_connect_errno()){ die("Error!"); }
				$stmt = mysqli_stmt_init($link);

				if(mysqli_stmt_prepare($stmt, $query)){
					mysqli_stmt_bind_param($stmt, "i", $_SESSION['user_number']);
					mysqli_stmt_execute($stmt);

					mysqli_stmt_store_result($stmt);
					mysqli_stmt_bind_result($stmt, $shared_admin); //sharedAdmin tells us the user's rank in shared
					mysqli_stmt_fetch($stmt);
					mysqli_stmt_close($stmt);
			                unset($link); unset($query);
					if(($shared_admin<1) || ($shared_admin>1)){
						//show default module if number is less than 2, aka just a mod or normal user
						$link = db_connect($database_url,$database_username,$database_password,$database_name);
				                $query = "SELECT name,location FROM shared_installations WHERE number=?";
				                $moduleNumber = $_SESSION['load_first'];
				                $stmt = mysqli_stmt_init($link);

				                if(mysqli_stmt_prepare($stmt, $query)){
				                        mysqli_stmt_bind_param($stmt, "i", $moduleNumber);
				                        mysqli_stmt_execute($stmt);

				                        mysqli_stmt_store_result($stmt);
				                        mysqli_stmt_bind_result($stmt, $moduleName, $modulePath);
				                        mysqli_stmt_fetch($stmt);
				                        mysqli_stmt_close($stmt);
				                        unset($link); unset($query);
				                }else{ //if stmt prepare fails:
							session_destroy(); //in case the user is trying to load an expired module
				                        die("Error Loading Website!");
				                }

				                if((!isset($modulePath)) || (strlen($modulePath)<1)){ session_destroy(); die("Error Loading Website!"); }
						//load the default module or error if it can't
				                include($root . $modulePath . "module.php");
					}else{
						//if number is not less than 2
						if($shared_admin==1){
							//if user is an admin, get admin login
							$_SESSION['is_admin_1'] = $shared_admin;
							include('./admin_login.php');
						}
					}
				}else{
					die("Error!");
				}
			} //end if the user hasn't gone through the admin login
		}else{ //if user isn't logged in
			die("ERROR! Login first!");
		} //end if user isn't logged in
	}else{ //if e isn't set
		//load the module based on our load_first session data; if not logged in, the default is the default module set in sessions
		$link = db_connect($database_url,$database_username,$database_password,$database_name);
		$query = "SELECT name,location FROM shared_installations WHERE number=?";
		$moduleNumber = $_SESSION['load_first'];
		$stmt = mysqli_stmt_init($link);

		if(mysqli_stmt_prepare($stmt, $query)){
			mysqli_stmt_bind_param($stmt, "i", $moduleNumber);
			mysqli_stmt_execute($stmt);

			mysqli_stmt_store_result($stmt);
			mysqli_stmt_bind_result($stmt, $moduleName, $modulePath);
			mysqli_stmt_fetch($stmt);
			mysqli_stmt_close($stmt);
			unset($link); //unset($query);
		}else{ //if stmt prepare fails:
			session_destroy(); //in case user is loading expired module
			die("Error Loading Website!");
		}

		if((!isset($modulePath)) || (strlen($modulePath)<1)){ session_destroy(); die("Error Loading Website!"); }
		include($root . $modulePath . "module.php");
	}//end if e isn't set
}//end if m isn't set or isn't a number
?>
