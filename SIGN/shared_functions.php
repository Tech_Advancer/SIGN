<?php
///////////////////////////////////////////////////////////////////////
//shared_functions.php
//Basic functions that all modules will likely need.
///////////////////////////////////////////////////////////////////////

if(isset($load_check)){

function db_connect($database_url,$database_username,$database_password,$database_name){
$mysqli_link = mysqli_connect($database_url,$database_username,$database_password,$database_name);
if(mysqli_connect_errno()){ die("<h2>Error loading website!</h2>"); }
return $mysqli_link;
}

// The next few functions are escape functions to prevent bad code from going into the database:
// I'm not sure if they are even needed since we are using mysqli, but I'll just leave it here to be safe.

function db_safe($value, $mysqli_link=NULL)
{
$value=iconv("UTF-8", "ISO-8859-1//TRANSLIT", $value);
$value=strip_tags($value);
$value=escapeshellcmd($value);
if(get_magic_quotes_gpc()) {$value = stripslashes($value);}
$value=trim($value);
  //This is just to be sure the database connection is open since mysqli_escape_string will return an empty string otherwise
  if(($mysqli_link!=NULL) && (is_resource($mysqli_link)) && (get_resource_type($mysqli_link) === 'mysql link')){ //code found on stackoverflow; question #3075116
   return mysqli_escape_string($mysqli_link,$value);
  }else{
   return $value;
  }
}

function displayable_db_safe($text, $mysqli_link=NULL){
if(get_magic_quotes_gpc()) {$text = stripslashes($text);}
$text=trim($text);

$text=iconv("UTF-8", "ISO-8859-1//TRANSLIT", $text);
$size=strlen($text);
for($i=0;$i<=$size-1;$i++) //break down the text character by character
{
	if((preg_match('/[^0-9A-Za-z ]/',$text{$i})) && (ord($text{$i})>=32) && (ord($text{$i})<=126)){
	$newtext.="&#" . ord($text{$i}) . ";"; //symbols of any kind are changed into html special characters to prevent bad code from running on user's side
	}else{
		if((ord($text{$i})==10) || (ord($text{$i})==9) || (ord($text{$i})==13) || (ord($text{$i})==11) || (ord($text{$i})==12))
		{
		$newtext.="&#" . ord($text{$i}) . ";";
		}else{
		$newtext.=$text{$i}; //only print if it is a normal letter or number
		}
	}
}
  //This is just to be sure the database connection is open since mysqli_escape_string will return an empty string otherwise
  if(($mysqli_link!=NULL) && (is_resource($mysqli_link)) && (get_resource_type($mysqli_link) === 'mysql link')){ //code found on stackoverflow; question #3075116
   return "'" . mysqli_escape_string($mysqli_link,$newtext) . "'"; //this just checks to be sure the db connection is open since escape_string will re$
  }else{
   return "'" . $newtext . "'";
  }

}


function get_ip() { //get user's ip to be used for bans
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                    return $ip;
                }
            }
        }
    }
}

}else{
if(headers_sent()){die(include($root . "/404.php"));} //if headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); //send headers if not sent already
die(include($root . "/404.php")); //die to custom 404 page
}
?>
