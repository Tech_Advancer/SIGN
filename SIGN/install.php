<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SIGN Installer</title>
<style type="text/css">
body{
text-align: center;
}
</style>
</head>
<body>

<h1>Install SIGN</h1>

<?php
///////////////////////
//
//  install.php
//  Not Included
//  Installs SIGN for
//  the first time.
//  DELETE AFTERWARDS!
//////////////////////

if((isset($_POST['phase'])) && (is_numeric($_POST['phase']))){
	$phase = $_POST['phase'];
}else{
	if((isset($_GET['phase'])) && (is_numeric($_GET['phase']))){
		$phase = $_GET['phase'];
	}else{
		$phase = 0;
	}
}
$message="";

if($phase==0){
	if((isset($_POST['dbUN'])) && (isset($_POST['dbName'])) && (isset($_POST['dbPW'])) && (isset($_POST['dbURL'])) && (isset($_POST['adminUserName'])) && (isset($_POST['adminPW'])) && (isset($_POST['adminPW2'])) && (isset($_POST['adminEmail'])) && (isset($_POST['hashCost']))){
	//if the form was submitted
		$dbUN = $_POST['dbUN'];
		$dbPW = $_POST['dbPW'];
		$dbURL = $_POST['dbURL'];
		$dbName = $_POST['dbName'];
		$userName = $_POST['adminUserName'];
		$password = $_POST['adminPW'];
		$passwordVerify = $_POST['adminPW2'];
		$email = $_POST['adminEmail'];
		$hashCost = $_POST['hashCost'];

		if((strlen($userName)<3) || (strlen($userName)>25)){ $message.='Your username MUST be between 3 and 25 characters long!<br>'; }
                if((strlen($password)<8) || (strlen($password)>70)){ $message.='Your password MUST be between 8 and 70 characters long!<br>'; }
                if(strcmp($password, $passwordVerify) !== 0){ $message.='Your password didn\'t match! It IS case sensitive!<br>'; }
		if(strlen($email)<3){ $message.='You must enter your email address!<br>'; }

		if(strlen($message)==0){
			//if there are no errors
			$link = mysqli_connect($dbURL,$dbUN,$dbPW,$dbName);
			if(mysqli_connect_errno($link)){ $message.='Couldn\'t connect to database.'; }

			if(strlen($message)==0){
				//last error check

				//DROP ALL IF EXISTS:
				$query = 'DROP TABLE IF EXISTS shared_installations';
                                if(!$result = mysqli_query($link, $query)){
                                        die("Error!<br>" . mysqli_error($link));
                                }
                                unset($query); unset($row); unset($result);

				$query = 'DROP TABLE IF EXISTS shared_settings';
                                if(!$result = mysqli_query($link, $query)){
                                        die("Error!<br>" . mysqli_error($link));
                                }
                                unset($query); unset($row); unset($result);

				$query = 'DROP TABLE IF EXISTS shared_users';
                                if(!$result = mysqli_query($link, $query)){
                                        die("Error!<br>" . mysqli_error($link));
                                }
                                unset($query); unset($row); unset($result);

				//CREATE TABLES:

				$query = 'CREATE TABLE shared_installations(number bigint(20) NOT NULL AUTO_INCREMENT,name longtext,location longtext NOT NULL, PRIMARY KEY(number))';
			        if(!$result = mysqli_query($link, $query)){
			                die("Error!<br>" . mysqli_error($link));
			        }
			        unset($query); unset($row); unset($result);

				$query = 'CREATE TABLE shared_settings(name text NOT NULL,intValue int(11),textValue longtext)';
			        if(!$result = mysqli_query($link, $query)){
			                die("Error!<br>" . mysqli_error($link));
			        }
			        unset($query); unset($row); unset($result);

				$lfName = "loadFirst";
				$loadFirst = -1;

				//INSERT loadFirst into shared settings, set to -1!!
				$query = 'INSERT INTO shared_settings(name,intValue) VALUES(?,?)';
                                $stmt = mysqli_stmt_init($link);
                                if(mysqli_stmt_prepare($stmt, $query)){
                                        mysqli_stmt_bind_param($stmt, "si", $lfName, $loadFirst);
                                        mysqli_stmt_execute($stmt);
                                        mysqli_stmt_close($stmt);
                                        unset($query);
                                }else{ //if stmt_prepare fails:
                                        die("Error!<br>" . mysqli_error($link));
                                }

				$query = 'CREATE TABLE shared_users(number bigint(20) NOT NULL AUTO_INCREMENT,username tinytext NOT NULL,password longtext NOT NULL,joinDate tinytext,lastLogin tinytext,emailAddress text NOT NULL,loadFirst bigint(20),website text,birthday tinytext,timeZone int(11),passwordReset longtext,accountActivationCode longtext,modNotes longtext,banReason longtext,ipList text,banYear smallint(4),banMonth tinyint(2),banDay tinyint(2),uniqueID longtext NOT NULL,sharedAdmin tinyint(1) NOT NULL DEFAULT 0,PRIMARY KEY(number))';
                                if(!$result = mysqli_query($link, $query)){
                                        die("Error!<br>" . mysqli_error($link));
                                }
                                unset($query); unset($row); unset($result);

				//CREATE Admin User Account:
                                $password = password_hash($password, PASSWORD_DEFAULT, ['cost' => $hashCost ]);
                                $joinDate = date("F j, Y");
				$uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);

				$query = 'INSERT INTO shared_users (username,password,joinDate,emailAddress,loadFirst,uniqueID,sharedAdmin) VALUES (?,?,?,?,?,?,?)';
			        $stmt = mysqli_stmt_init($link);
			        if(mysqli_stmt_prepare($stmt, $query)){
					$sharedAdmin=1;
			                mysqli_stmt_bind_param($stmt, "ssssisi", $userName, $password, $joinDate, $email, $loadFirst, $uid, $sharedAdmin);
			                mysqli_stmt_execute($stmt);
					$userNumber = mysqli_insert_id($link);
			                mysqli_stmt_close($stmt);
			                unset($query);
			        }else{ //if stmt_prepare fails:
			                die("Error!<br>" . mysqli_error($link));
			    	}
				mysqli_close($link);

				$_SESSION['user_number']=$userNumber;
		                $_SESSION['user_name']=$userName;

				$phase=1;

			} //end last error check
		} //end if there are no errors

	} //end if the form was submited
} //end of Phase 0

if($phase==0){ //check again seperately for second run; do NOT combine with above
?>
<p>Welcome to the SIGN installer. Running this script will install Project SIGN on your server. Before you start, setup a table and user with access to that table in your database.</p>
<b>Warning! This will overwrite and delete any previous installation of SIGN!</b>
<br><br>

<?php if((isset($message)) && (strlen($message)>0)){ ?>
<br>
<div style="border: solid 2px red; color: red; font-weight: bold;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<form action="./install.php" method="POST" autocomplete="off">
<label>Database URL: <input type="text" value="localhost" name="dbURL"></label><br>
<label>Database User's Name: <input type="text" name="dbUN"></label><br>
<label>Database User's Password: <input type="password" name="dbPW"></label><br>
<label>Database Name: <input type="text" name="dbName"></label><br>
<hr width="70%;">
<p>Setup your admin account on SIGN too:</p>
<label>Username: <input type="text" name="adminUserName"></label><br>
<label>Email Address: <input type="text" name="adminEmail"></label><br>
<label>Password: <input type="password" name="adminPW"></label><br>
<label>Password (Again): <input type="password" name="adminPW2"></label><br>
<hr width="70%;">
<label>Hash Cost: <input type="text" value="11" name="hashCost"></label><br><br>
<input type="submit" value="Install">
</form>
<?php
} //end of phase 0

/////////////////////////////// PHASE 1 ////////////////////////

if($phase==1){
?>

<p>Below is your new configuration file. Copy the contents and make a new file called "shared_variables.php".
You can put the file in the SIGN root directory, but for safety, it is best to place it in a non web-facing directroy.
Then go to SIGN's index.php file in root. Change the variable "shared_variables_location" to the absolute directory of "shared_variables.php".</p>

<p>Click the link below when you finish. <b>Warning, you can't regenerate this! Don't lose it!</b></p>

<textarea cols="100" rows="20">
<?php echo
'<?php
///////////////////////////////////////////////////////////////////////' . "\n" .
'//shared_variables.php' . "\n" .
'//Sets very important variables for database connections and other' . "\n" .
'//  security critical items.' . "\n" .
'///////////////////////////////////////////////////////////////////////' . "\n" .
'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////' . "\n" .
'//' . "\n" .
'//   FOR SECURITY THIS PAGE SHOULD BE MOVED OUT OF THE WWW FACING DIRECTORY AND index.php SHOULD BE EDITED TO ITS NEW LOCATION!!!' . "\n" .
'//' . "\n" .
'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////' . "\n" .
'
if(isset($load_check)){ //if included from another php source

$database_url=\'' . $dbURL . '\'; //database location/url
$database_name=\'' . $dbName . '\'; //database name
$database_username=\'' . $dbUN . '\';     //username for the database
$database_password=\'' . $dbPW . '\';//database password


// Settings for password hashes:
$hashOptions = [
    \'options\' => [\'cost\' => ' . $hashCost . '],
    \'algo\' => PASSWORD_DEFAULT,
    \'hash\' => null
];


}else{ //if NOT included from another php source
if(headers_sent()){die(include($root . "/404.php"));} //if headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); //send headers if not sent already
die(include($root . "/404.php")); //die to custom 404 page
}
?>';
?>
</textarea><br><br>

<form action="./install.php" method="POST">
<input type="hidden" name="phase" value="2">
<input type="hidden" name="adminUN" value="<?php echo $userName; ?>">
<input type="hidden" name="adminNum" value="<?php echo $userNumber; ?>">
<input type="hidden" name="dbURL" value="<?php echo $dbURL; ?>">
<input type="hidden" name="dbName" value="<?php echo $dbName; ?>">
<input type="hidden" name="dbUN" value="<?php echo $dbUN; ?>">
<input type="hidden" name="dbPW" value="<?php echo $dbPW; ?>">
<input type="hidden" name="hash" value="<?php echo $hashCost; ?>">
<input type="submit" value="Click to Continue Install">
</form>

<?php
} //end of phase 1

if($phase==2){

	if(!isset($_SESSION['user_number'])){
		$_SESSION['user_number']=$_GET['num'];
	}
	if(!isset($_SESSION['user_name'])){
		$_SESSION['user_name']=$_GET['un'];
	}

	if((isset($_POST['adminUN'])) && (isset($_POST['adminNum'])) && (isset($_POST['dbURL'])) && (isset($_POST['dbName'])) && (isset($_POST['dbUN'])) && (isset($_POST['dbPW'])) && (isset($_POST['hash']))){
		$userName = $_POST['adminUN'];
		$userNumber = $_POST['adminNum'];
		$database_url = $_POST['dbURL'];
		$database_name = $_POST['dbName'];
		$database_username = $_POST['dbUN'];
		$database_password = $_POST['dbPW'];
		$hash = $_POST['hash'];
		$hashOptions = [
  		  'options' => ['cost' => $hash],
		    'algo' => PASSWORD_DEFAULT,
		    'hash' => null
		];
	}

	$load_check = 1;
	if(!include_once("shared_functions.php")){
	//These error messages need to be updated later to use some sort of language file for multiple language support:
	die("Unable to find shared_functions.php.<br>Make sure install.php is in SIGN's root directory!");
	}

	//Temporarily Grant Admin Access to install module:
	$admin_load_check = $_SESSION['user_number'];
	$_SESSION['is_admin_1'] = 3;
	$_SESSION['is_admin_2'] = 3;

	$root = getcwd();
	$linkBack = './install.php?phase=2';

	if(isset($_POST['postCheck'])){ //loop back from installer
		$load = false;
                $moduleLocation = $_POST['moduleLocation'];
                include('.' . $moduleLocation . 'install.php');
	}else{//if first run
	if((isset($_POST['moduleLocation'])) && (strlen($_POST['moduleLocation'])>0)){ //if the form was submitted
	        if(file_exists("." . $_POST['moduleLocation'] . "install.php")){
	                $load = false;
	                $moduleLocation = $_POST['moduleLocation'];
	                include('.' . $moduleLocation . 'install.php');
	        }else{
	                $message="Installation or install.php not found at " . $_POST['moduleLocation'];
	        }
	}else{ //if the form wasn't submitted:
?>

<?php if((isset($message)) && (strlen($message)>0)){ ?>
<br>
<div style="border: solid 2px red; color: red; font-weight: bold;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<form action="./install.php" method="POST" autocomplete="off">
<label>Module File Path: <input type="text" name="moduleLocation" value="/website/"></label><br>
<input type="hidden" name="phase" value="2">
<input type="hidden" name="passBack" value="1">
<input type="hidden" name="adminUN" value="<?php echo $userName; ?>">
<input type="hidden" name="adminNum" value="<?php echo $userNumber; ?>">
<input type="hidden" name="dbURL" value="<?php echo $database_url; ?>">
<input type="hidden" name="dbName" value="<?php echo $database_name; ?>">
<input type="hidden" name="dbUN" value="<?php echo $database_username; ?>">
<input type="hidden" name="dbPW" value="<?php echo $database_password; ?>">
<input type="hidden" name="hash" value="<?php echo $hash; ?>">
Example: <b>/path/path2/</b><br>The starting and ending / are required!
<br><br>
<input type="submit" value="Install">
</form>

<?php
	} //end if the form wasn't submitted
	} //end first run, if not looping back
} //end of phase 2


?>
</body>
</html>
