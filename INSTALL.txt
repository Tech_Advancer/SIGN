WARNING: SIGN is NOT ready for use in real environments yet! However, a plan to make it ready for real environments has been made.

REQUIREMENTS:
In order to install SIGN you will need PHP 5.5+ and mySQL 5.5+ with the ability to grant a user full access to a new table.

Steps:
-Download SIGN and a module. Modules can be found at:

  Website Module: https://gitlab.com/Tech_Advancer/Website-Module-For-SIGN
  Forum Module: https://gitlab.com/Tech_Advancer/Forum-Module-For-SIGN

-Upload the contents of the SIGN folder to the location of your choice.
-Upload the entire folder of either the website or forum folder from the modules above to the same directory you have SIGN's contents in. You can upload both as well.
-Create a new table in mySQL for SIGN.
-Create a user with full access to that table.
-Begin the install by pointing your web browser to install.php in SIGN. ( Located in root/install.php )
-Follow the on-screen instructions.