Project SIGN is a module loading system for webservers. It allows for quick and easy deployment of websites, forums, and other systems. 

Project SIGN uses PHP 5.5+ and mySQL 5.5+ to load modules stored in accompanying folders. A module is just a name for the code that runs the website, forum, or other system. With this, admins can quickly deploy many different systems with minimal effort. Admins can even deploy the same module many times over.

========================================

The OOPsign branch is the current main working branch! All of SIGN is being rewritten in OOP PHP. The current code here is dated and NOT secure! (...it is also messy, I was more of a newbie when I wrote it.)
No further work will be done on master until OOPsign merges in. That won't happen until SIGN, the website module, and the forum module are all OOP ready.

Checkout the OOPsign branch and see the BRANCH-INFO.txt file in it for more information.

========================================

This project was built as a school project by Tech_Advancer and is in no way ready for real world deployment. There are many things to do before it is ready.


Quick To-Do List (more will be added later):

-The whole project needs to be rewritten in an object-oriented fastion; it wasn't originally in order to finish it faster;
    the object-oriented code should be encapsulated properly in order to keep modules from being able to execute
    data in the shared SIGN tables and to keep module's themes from running any problematic code (more info on that will be given in a later commit)

-Language Files need to be implemented so admins can chose what language they'd like

-Themes need to be created; the current ones are only wire-frame styes to hold example code

-Installation and admin panels need to be fixed up so that it is easier to setup. The current version feels like it was made for engineers to work on.

-Instructions need to be laid out to make it easier for others to jump into the code and make alterations as they need
